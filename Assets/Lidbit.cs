﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Lidbit : MonoBehaviour {

    public enum Emotion
    {
        Joy = 0,
        Fear = 1,
        Anger = 2,
        Grief = 3,
        Disgust = 4
    } /* a list of the emotions */

    private float speed;
    private float bravery;

    public Emotion currentEmotion = Emotion.Joy;
    public Vector2 currentVector = new Vector2(0f, 0f);

	// Use this for initialization
	void Start () {
        speed = Random.Range(0.1f, 1.5f);
        bravery = Random.Range(0.1f, 1.5f);
	}
	
	// Update is called once per frame
	void Update () {

        // TODO: determine emotional state changes 


        // determine velocity changes
        Vector2 vel = GetVelocity(currentEmotion);
        Move(vel.x, vel.y); // and then move
    }

    Vector2 GetVelocity (Emotion currentEmotion)
    {
        switch (currentEmotion)
        {
            case Emotion.Joy:
                if (currentVector.x <= speed - 0.1f)
                {
                    currentVector.x += 0.1f;
                }
                if (currentVector.y <= speed - 0.1f)
                {
                    currentVector.y += 0.1f;
                }
                return currentVector;
            default:
                return currentVector;
        }
    }

    // move the character this amount
    void Move (float x, float z)
    {
        transform.position = new Vector3(x, 0, z);
    }
}
